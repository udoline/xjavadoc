### The XJavaDoc project

This project illustrates how [base XJavaDoc](https://sourceforge.net/projects/xdoclet/files/xjavadoc) applications can build and use withhin Java-SE 8. 

#### Quick installation

To submit a GitHub Pull Request you'll need to fork the
[repository](https://gitlab.com/udoline/xjavadoc), clone your fork to do the work:

```
$ git clone https://gitlab.com/udoline/xjavadoc.git
```

or you download it via [xjavadoc-master.zip](https://gitlab.com/udoline/xjavadoc/-/archive/master/xjavadoc-master.zip)

##### Building this project #####

first time you must run 

```
$ cd xjavadoc
$ gradlew ant_init
```

This will copy all nessasary jars to the local '../xjavadoc/lib'.

And now you can use Ant, Maven or Gradle to build this project.
```
$ mvnw clean verify
$ gradlew build
```

Now _XJavaDoc-1.8.0.jar_ is build and it locate into the subdiretory.

##### Verify your local build #####

Run all known tests.

```
cd xjavadoc
$ gradlew ant_junit
```

##### Resolving touble with your local build  #####

Please try 

```
$ cd xjavadoc
$ gradlew ant_init -i
```

You can analyse the classpath assembling via 

```
$ cd xjavadoc
$ gradlew printSourceSetInformation
```

##### Support Docker build #####
[Dockerfile](https://gitlab.com/udoline/xjavadoc/-/blob/master/Dockerfile)

```
docker rm -f  xjavadoc:latest
docker build -t  xjavadoc:latest .
docker run -it --entrypoint=bash xjavadoc:latest
```

## What can you do with them? ##

### Deploy the relevant JAR ###
You could deploy this artifacts in the company nexus of your third party libraries section, or whatever you want

```
mvn deploy:deploy-file   \
 -DgroupId=xdoclet -DartifactId=xjavadoc -Dversion=1.8.0  \
 -Dpackaging=jar -Dfile=target\xjavadoc-1.8.0.jar  -DrepositoryId=thirdparty  \
 -Dfiles=xjavadoc-1.8.0-sources.jar -Dtypes=jar -Dclassifiers=sources  \
 -Durl=http://your-company-nexus.de/nexus/repository/thirdparty
```

### How to consume this JAR ###

E.g. now you can replace the obsolete 'xdoclet:xjavadoc:1.5-20050611' with the fresh build and deployed version 'xdoclet:xjavadoc:1.8.0' for JDK8.

#### Maven dependency management section ####

Here is a simple sample with Struts 1.3.10

```
<!-- Xdoclet - configuration generator (struts|taglibs) -->
<dependency>
	<groupId>xdoclet</groupId>
	<artifactId>xdoclet-web-module</artifactId>
	<version>1.2.3</version>
	<scope>provided</scope>
	<exclusions>
		<exclusion>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
		</exclusion>
	</exclusions>
</dependency>
<dependency>
	<groupId>xdoclet</groupId>
	<artifactId>xdoclet-java-module</artifactId>
	<version>1.2.3</version>
	<scope>provided</scope>
</dependency>
<dependency>
	<groupId>xdoclet</groupId>
	<artifactId>xdoclet-de-locale</artifactId>
	<version>1.2.3</version>
	<scope>provided</scope>
</dependency>
<dependency>
	<groupId>xdoclet</groupId>
	<artifactId>xdoclet-hibernate-module</artifactId>
	<version>1.2.3</version>
	<scope>provided</scope>
	<exclusions>
		<exclusion>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
		</exclusion>
	</exclusions>
</dependency>
<dependency>
	<groupId>xdoclet</groupId>
	<artifactId>xdoclet-ejb-module</artifactId>
	<version>1.2.3</version>
	<scope>provided</scope>
	<exclusions>
		<exclusion>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
		</exclusion>
	</exclusions>
</dependency>
<dependency>
	<groupId>xdoclet</groupId>
	<artifactId>xdoclet-apache-module</artifactId>
	<version>1.2.3</version>
	<scope>provided</scope>
</dependency>
<dependency>
	<groupId>xdoclet</groupId>
	<artifactId>xdoclet-xdoclet-module</artifactId>
	<version>1.2.3</version>
	<scope>provided</scope>
</dependency>
<dependency>
	<groupId>xdoclet</groupId>
	<artifactId>xjavadoc</artifactId>
	<version>1.8.0</version> <!-- OBSOLETE <version>1.5-20050611</version> -->
	<scope>provided</scope>
</dependency>
<!-- EO: Xdoclet - configuration generator (struts|taglibs) -->
```

#### Consume it via Maven plugin execution ####

Here is a simple sample Ant task for building something for your webapp which is running with Struts 1.3.10

```
<!-- Erzeugen der Struts|JspTag-Konfiguration für die WebApplication -->
<plugin>
	<artifactId>maven-antrun-plugin</artifactId>
	<executions>
		<execution>
			<id>your-project-xdoclet-run</id>
			<phase>generate-sources</phase>
			<goals>
				<goal>run</goal>
			</goals>
			<configuration>
				<tasks>
					<echo message="(xdoclet) webdoclet generate-sources ... " />

					<taskdef classname="xdoclet.modules.web.WebDocletTask" classpathref="maven.compile.classpath" name="webdoclet">
					</taskdef>
					<webdoclet destdir="${project.build.directory}/generated/WEB-INF"
					        mergedir="src/main/xdoclet/struts" verbose="true" force="true"
					        excludedtags="@version,@author,@since,@Override,@inheritDoc,@see,@return,@exception" addedTags="">
						<fileset dir="${project.build.sourceDirectory}">
							<include name="**/*.java" />
						</fileset>
						<strutsconfigxml validatexml="true" destinationFile="struts-config.xml" version="1.2" />
						<strutsvalidationxml version="1.2" />
						<jsptaglib shortname="YourProject" filename="your-project.tld" jspversion="1.2" taglibversion="1.2" 
                                   description="Web Applikation YourProject spezifische JSP-Taglib für die JSP-Dateien" 
                                   destDir="${project.build.directory}/generated/WEB-INF">
						</jsptaglib>
					</webdoclet>
					<copy todir="${project.build.outputDirectory}">
						<fileset includes="**/*.properties" dir="${project.build.sourceDirectory}">
						</fileset>
					</copy>
				</tasks>
				<sourceRoot>
					${project.build.sourceDirectory}
				</sourceRoot>
			</configuration>
		</execution>
	</executions>
</plugin>
```

## License

[The content of this repository is licensed under the European Union Public License 1.2.](https://choosealicense.com/licenses/eupl-1.2/)

Copyright (c) 2021 udoline (1995)
