# using multistage docker build
# ref: https://docs.docker.com/develop/develop-images/multistage-build/
    
# temp container to build using gradle
# https://hub.docker.com/_/gradle 
FROM gradle:7.2.0-jdk8 AS BUILD_IMAGE_GRADLE
ENV APP_HOME=/usr/app/xjavadoc
WORKDIR $APP_HOME
COPY build.xml build.gradle settings.gradle  $APP_HOME
COPY etc     $APP_HOME/etc
COPY javacc  $APP_HOME/javacc
COPY junit   $APP_HOME/junit
COPY test    $APP_HOME/test
COPY src     $APP_HOME/src
COPY gradle  $APP_HOME/gradle
  
COPY --chown=gradle:gradle . /home/gradle/src
USER root
RUN chown -R gradle /home/gradle/src
    
### build action
RUN gradle ant_init
RUN cp -fv /home/gradle/src/lib/x*.jar $APP_HOME/lib/
RUN gradle printSourceSetInformation
RUN gradle ant_junit
RUN gradle build
RUN chmod 644 /home/gradle/src/lib/* $APP_HOME/lib/*  $APP_HOME/build/libs/*

RUN ls -lias $APP_HOME/build/libs/[Xx][Jj]*[Dd]*.jar && echo Finished ... OK || echo Finished ... FAILED

COPY . .

# 
# docker run xjavadoc:latest cat /usr/app/xjavadoc/build/libs/[Xx][Jj]*[Dd]*.jar
