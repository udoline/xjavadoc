/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates XTag instances.
 *
 * @author    Aslak Helles&#x00F8;y
 * @since   10. februar 2002
 */
public final class XTagFactory
{
	private static final Logger log = LoggerFactory.getLogger(XTagFactory.class);

	/**
	 * Maps tag name to XTag class.
	 */
	private final Map _tagClasses = new HashMap();
	private boolean _isValidating = false;

    public XTagFactory() {
		// ignore standard tags. See:
		// http://java.sun.com/j2se/1.4.1/docs/tooldocs/windows/javadoc.html#javadoctags
		setIgnoredTags( "author,deprecated,exception,param,return,see,serial,serialData,serialField,since,throws,version" );
	}

	public boolean isValidating()
	{
		return _isValidating;
	}

	public void setValidating( boolean isValidating )
	{
		_isValidating = isValidating;
	}

	/**
	 * Set the name of the tags that shouldn't be validated against.
	 *
	 * @param tags as CSV
	 */
	public void setIgnoredTags( String tags )
	{
		StringTokenizer st = new StringTokenizer( tags, "," );

		while( st.hasMoreTokens() )
		{
			registerTagClass( st.nextToken(), DefaultXTag.class );
		}
	}

	/**
	 * Creates a new XTag. If a special tag class has been previously registeres,
	 * an instance of the corresponding class will be returned. This allows for
	 * special tag implementations.
	 *
	 * @param tagName                  name of the tag, without the '@'
	 * @param text                     content of the tag. Will be parsed into
	 *      attributes.
	 * @param doc It describes what the parameter could be hold ...
	 * @param lineNumber It describes what the parameter could be hold ...
	 * @return                         an instance of XTag
	 * @throws UnknownTagException It gives an information to the programmer that there may occur an exception.
	 * @throws SecurityException  It gives an information to the programmer that there may occur an exception.
	 * @throws NoSuchMethodException  It gives an information to the programmer that there may occur an exception.
	 * @throws InvocationTargetException  It gives an information to the programmer that there may occur an exception.
	 * @throws IllegalArgumentException  It gives an information to the programmer that there may occur an exception.
	 * @throws TagValidationException  if validation is activated and an unknown
	 *      tag was encountered.
	 */
	public XTag createTag( String tagName, String text, XDoc doc, int lineNumber ) throws UnknownTagException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
	{
		tagName = XDoc.dotted( tagName );

		// Let's see if there is a custom class for that tag
		Class tagClass = ( Class ) _tagClasses.get( tagName );
		DefaultXTag tag;

		if( tagClass != null )
		{
			try
			{
				tag = ( DefaultXTag ) tagClass.getDeclaredConstructor().newInstance();
			}
			catch( InstantiationException e )
			{
				log.warn("Instantiation", e);
				throw new IllegalStateException( e.getMessage() );
			}
			catch( IllegalAccessException e )
			{
				log.warn("IllegalAccess", e);
				throw new IllegalStateException( e.getMessage() );
			}
		}
		else
		{
			tag = new DefaultXTag();
		}
		tag.init( tagName, text, doc, lineNumber );

		// Throw validation ex if validation is on and the tag is unknown
		if( _isValidating && ( tagClass == null ) )
		{
			throw new UnknownTagException( tag );
		}
		return tag;
	}

	public void registerTagClass( String tagName, Class tagClass )
	{
        Class old = (Class) _tagClasses.get( XDoc.dotted(tagName) );
        if( old != null ) {
            throw new IllegalStateException( "The tag @" + XDoc.dotted(tagName) +
                    " has already been mapped to " + old.getName() +
                    ". Can't reregister it to " + tagClass.getName());
        }
		_tagClasses.put( XDoc.dotted( tagName ), tagClass );
	}
}
