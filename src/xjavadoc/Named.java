/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc;

/**
 * Everything that can have a name implements this interface
 *
 * @author    Aslak Helles&#x00F8;y
 * @since   16. oktober 2002
 */
public interface Named
{
	/**
	 * Get name
	 *
	 * @return   name
	 */
	String getName();
}
