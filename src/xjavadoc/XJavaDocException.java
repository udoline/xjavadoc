/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc;

/**
 * @author    Aslak Helles&#x00F8;y
 * @since   19. februar 2002
 */
public class XJavaDocException extends Exception
{

	/**
	 * TODO   Describe the field
	 */
	private Throwable  _source;

	/**
	 * Describe what the XJavaDocException constructor does
	 *
	 * @param source   It describes what the parameter could be hold ...
	 * TODO   Write javadocs for constructor
	 * TODO   Write javadocs for method parameter
	 */
	public XJavaDocException( Throwable source )
	{
		this( source.getMessage() );
		_source = source;
	}

	/**
	 * Describe what the XJavaDocException constructor does
	 *
	 * @param message  It describes what the parameter could be hold ...
	 * TODO   Write javadocs for constructor
	 * TODO   Write javadocs for method parameter
	 */
	public XJavaDocException( String message )
	{
		super( message );
	}

	/**
	 * Gets the Source attribute of the XJavaDocException object
	 *
	 * @return   The Source value
	 */
	public Throwable getSource()
	{
		return _source;
	}
}

