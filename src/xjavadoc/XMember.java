/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc;

/**
 * This is just a marker interface to be able to distinguish between class
 * members and classes. Used in XDoc.superdoc().
 *
 * @author    Aslak Helles&#x00F8;y
 * @since   12. mars 2002
 */
public interface XMember extends XProgramElement
{
}
