/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc;

/**
 * Describe what this class does
 *
 * @author         Aslak Helles&#x00F8;y
 * @since        7. mars 2002
 */
interface JavaParser
{
	/**
	 * Populates the class by parsing its source.
	 *
	 * @param sourceClass         the XClass object to populate
	 * @throws ParseException  if the parsed file is not compliant with the Java
	 *      grammar
	 */
	void populate( SourceClass sourceClass ) throws ParseException;

	/**
	 * Gets the Token attribute of the JavaParser object.
	 *
	 * @param index    It describes what the parameter could be hold ...
	 * @return         The Token value
	 */
	Token getToken( int index );
}