/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc.event;

/**
 * Describe what this class does
 *
 * @author         Aslak Helles&#x00F8;y
 * @since        30. januar 2002
 * TODO   Write javadocs for interface
 */
public interface XDocListener extends java.util.EventListener
{
	/**
	 * Describe what the method does
	 *
	 * @param event    It describes what the parameter could be hold ...
	 * TODO   Write javadocs for method
	 * TODO   Write javadocs for method parameter
	 */
	void docChanged( XDocEvent event );
}
