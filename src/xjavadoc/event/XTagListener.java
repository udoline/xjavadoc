/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc.event;

import java.util.EventListener;

/**
 * Describe what this class does
 *
 * @author         Aslak Helles&#x00F8;y
 * @since        30. januar 2002
 * TODO   Write javadocs for interface
 */
public interface XTagListener extends EventListener
{
	/**
	 * notify abou tag change
	 *
	 * @param event    It describes what the parameter could be hold ...
	 * TODO   Write javadocs for method
	 * TODO   Write javadocs for method parameter
	 */
	void tagChanged( XTagEvent event );
}
