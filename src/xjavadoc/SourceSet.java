/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc;

import xjavadoc.filesystem.AbstractFile;

import java.io.Serializable;

/**
 * This interface represents a set of Java source files.
 *
 * @author  <a href="mailto:dim@bigpond.net.au">Dmitri Colebatch</a>
 * @since   October 4, 2002
 */
public interface SourceSet extends Serializable
{

	/**
	 * @return TOOD description
	 */
	AbstractFile[] getFiles();

	/**
	 * @param qualifiedName TOOD description
	 * @return TOOD description
	 */
	AbstractFile getSourceFile( String qualifiedName );

	/** 
	 * @param i TOOD description
	 * @return TOOD description
	 */
	String getQualifiedName( int i );

	/**
	 * @return TOOD description
	 */
	int getSize();
}
