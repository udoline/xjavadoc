/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc.filesystem;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.io.Reader;
import java.io.OutputStream;
import java.io.FileNotFoundException;

/**
 * An interface that allows XJavadoc to read and write from any
 * source/destination and not just files.
 *
 * @author <a href="dim@bigpond.net.au">Dmitri Colebatch</a>
 * @since September 25, 2002
 */
public interface AbstractFile {

	/**
	 * @return Obtain a reader for the file.
	 *
	 * @throws IOException It gives an information to the programmer that there may occur an exception.
	 */
	Reader getReader() throws IOException;

	/**
	 * @return Obtain a writer for the file.
	 *
	 * @throws IOException It gives an information to the programmer that there may occur an exception.
	 */
	Writer getWriter() throws IOException;

	/**
	 * @return Determine if the file is writable or not.
	 */
	boolean isWriteable();

	/**
	 * @return Get the last modified timestamp of the file, or 0 if not available.
	 */
	long lastModified();

	/**
	 * @return Get an output stream for the file.
	 * 
	 * @throws IOException It gives an information to the programmer that there may occur an exception.
	 */
	OutputStream getOutputStream() throws IOException;

	String getPath();

	/**
	 * @param encoding specification
	 * 
	 * @return Obtain a reader for the file.
	 * 
	 * @throws UnsupportedEncodingException It gives an information to the programmer that there may occur an exception.
	 * @throws FileNotFoundException It gives an information to the programmer that there may occur an exception.
	 */
	Reader getReader(String encoding) throws UnsupportedEncodingException, FileNotFoundException;
}
