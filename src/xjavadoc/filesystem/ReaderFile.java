/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc.filesystem;

import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.io.Writer;
import java.io.OutputStream;
import java.io.FileNotFoundException;
import org.apache.commons.lang3.NotImplementedException;

/**
 * @since September 25, 2002
 */
public class ReaderFile implements AbstractFile {
	private Reader file;

	public ReaderFile(Reader file) {
		this.file = file;
	}

	public Reader getReader() throws IOException {
		return file;
	}

	public Reader getReader(String enc) throws UnsupportedEncodingException, FileNotFoundException {
		return file;
	}

	public Writer getWriter() throws IOException {
		throw new NotImplementedException ("TODO: writer");
	}

	public boolean isWriteable() {
		return false;
	}

	public OutputStream getOutputStream() throws FileNotFoundException {
		throw new NotImplementedException ("TODO: outputStream");
	}

	public String getPath() {
		throw new NotImplementedException ("TODO: path");
	}

	public long lastModified() {
		return Long.MIN_VALUE;
	}
}
