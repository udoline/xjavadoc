package xjavadoc;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
import junit.framework.TestCase;
import xjavadoc.ant.XJavadocTask;
import xjavadoc.filesystem.FileSourceSet;

/**
 * @author udoline 
 * @since 06. Aug. 2021
 */
public class XJavaDocTestJ18 extends TestCase {
	
	private static final Logger log = LoggerFactory.getLogger(XJavaDocTestJ18.class);
	
	private final XJavaDoc _xJavaDoc = new XJavaDoc();

	public void setUp() throws Exception {
		File dir = new File("test");
		HashMap propertyMap = new HashMap();

		propertyMap.put("name", "xjavadoc");
		propertyMap.put("version", "1.0");

		_xJavaDoc.reset(true);
		_xJavaDoc.setPropertyMap(propertyMap);
		_xJavaDoc.addSourceSet(new FileSourceSet(dir));
	}

	/**
	 * A unit test for JUnit
	 */
	public void testReadHello3() {
		XClass clazz = _xJavaDoc.getXClass("Hello3");

		XDoc doc = clazz.getDoc();

		assertEquals("Bla bla yadda yadda", doc.getFirstSentence());

		XTag tag = (XTag) doc.getTags().get(0);

		assertEquals("foo.bar", tag.getName());
		assertEquals("beer=\"good\" tea=\"bad\"", tag.getValue());

		doc = clazz.getMethod("getNonsense()").getDoc();
		tag = (XTag) doc.getTags().get(0);
		assertEquals("This is getNonsense.", doc.getFirstSentence());
		assertEquals("star.wars", tag.getName());
		assertEquals("is=\"a crappy movie\" but=\"I went to see it anyway\"", tag.getValue());

		tag = (XTag) doc.getTags().get(1);
		assertEquals("empty.tag", tag.getName());
		assertEquals("", tag.getValue());

		doc = clazz.getMethod("whatever(java.lang.String[][],int)").getDoc();
		tag = (XTag) doc.getTags().get(0);
		assertEquals("Mr.", doc.getFirstSentence());
		assertEquals("more", tag.getName());
		assertEquals("testdata, bla bla", tag.getValue());

		tag = (XTag) doc.getTags().get(1);
		assertEquals("maybe", tag.getName());
		assertEquals("this=\"is\" only=\"testdata\"", tag.getValue());
	}

	/**
	 * A unit test for JUnit
	 */
	public void testPrivateField() {

		XClass clazz = _xJavaDoc.getXClass("Hello3");
		XField privateField = clazz.getField("privateField");
		XDoc doc = privateField.getDoc();

		assertEquals("Braba papa, barba mama, baraba brother, barba sister", doc.getFirstSentence());
		assertEquals("privateField", privateField.getName());
		assertEquals(0, privateField.getDimension());
		assertEquals("java.lang.String", privateField.getType().getQualifiedName());
		assertEquals("java.lang.String", privateField.getType().getTransformedQualifiedName());
		assertEquals("String", privateField.getType().getTransformedName());
		assertTrue(privateField.isPrivate());
	}

	public void testPublicField() {

		XClass clazz = _xJavaDoc.getXClass("Hello3");
		XField privateField = clazz.getField("publicField");

		assertEquals("publicField", privateField.getName());
		assertEquals(0, privateField.getDimension());
		assertEquals("java.lang.String", privateField.getType().getQualifiedName());
		assertEquals("java.lang.String", privateField.getType().getTransformedQualifiedName());
		assertEquals("String", privateField.getType().getTransformedName());
		assertTrue(privateField.isPublic());
		assertTrue(privateField.isFinal());
		assertFalse(privateField.isPrivate());
	}

	public void testInheritedFields() {
		XClass clazz = _xJavaDoc.getXClass("Goodbye2");
		// get fields from superclass too
		Collection fields = clazz.getFields(true);

		XClass sc = clazz.getSuperclass();
		log.debug("\nSC=" + sc + " SCfsize=" + sc.getFields().size() + " GBfsize=" + clazz.getFields().size()
				+ " GBfsizeInher=" + clazz.getFields(true).size());

		// one from Goodbye2 and two from Hello3
		assertEquals(3, fields.size());

		for (Iterator i = fields.iterator(); i.hasNext();) {
			XField field = (XField) i.next();

			// The field name should not be "privateField"
			assertTrue(!"privateField".equals(field.getName()));
		}
	}

	public void testFirstSentence() {
		XClass clazz = _xJavaDoc.getXClass("Hello3");
		XMethod method = clazz.getMethod("firstMethod()");
		XDoc doc = method.getDoc();
		log.debug("FM=" + method);
		log.debug("FMDoc=" + doc);

		assertEquals("This shouldn't be the first sentence.This one should be.", doc.getFirstSentence());
	}

	/**
	 * Read a class, get a method, modify a tag parameter, reread it and see if
	 * change was successful.
	 *
	 * @throws Exception It gives an information to the programmer that there may occur an exception.
	 */
	public void testGetMethodParameterValue() throws Exception {
		_xJavaDoc.setUseNodeParser(true);

		XClass clazz = _xJavaDoc.getXClass("Hello3");
		XMethod method = clazz.getMethod("whatever(java.lang.String[][],int)");
		XDoc doc = method.getDoc();

		assertEquals("is", doc.getTagAttributeValue("maybe", "this", true));

		File testDir = new File("target/saved/testGetMethodParameterValue");
		String fileName = clazz.save(testDir);
	}

	public void testModifyMethodParameterValue() throws Exception {
		_xJavaDoc.setUseNodeParser(true);

		XClass clazz = _xJavaDoc.updateMethodTag("Hello3", "whatever(java.lang.String[],int)", "numbers", "three",
				"trois", 0);
		File testDir = new File("target/saved/testModifyMethodParameterValue");
		String fileName = clazz.save(testDir);
	}

	public void testAddCommentToCommentedClass() throws Exception {
		_xJavaDoc.setUseNodeParser(true);

		XClass clazz = _xJavaDoc.updateClassTag("Hello3", "here", "is", "a tag for ya", 0);
		File testDir = new File("target/saved/testAddCommentToCommentedClass");
		String fileName = clazz.save(testDir);
	}

	public void testInnerClass() throws Exception {
		XClass clazz = _xJavaDoc.getXClass("Hello3");
		Collection innerClasses = clazz.getInnerClasses();

		assertEquals(5, innerClasses.size());

		Iterator i = innerClasses.iterator();

		XClass innerClass = (XClass) i.next();

		// First enum
		assertEquals("Hello3.Planet", innerClass.getQualifiedName());
		assertEquals("Hello3$Planet", innerClass.getTransformedName());
		assertEquals("Hello3$Planet", innerClass.getTransformedQualifiedName());

		innerClass = (XClass) i.next();

		// 2 enum
		assertEquals("Hello3.Flag", innerClass.getQualifiedName());
		assertEquals("Hello3$Flag", innerClass.getTransformedName());
		assertEquals("Hello3$Flag", innerClass.getTransformedQualifiedName());

		innerClass = (XClass) i.next();

		// 3 enum
		assertEquals("Hello3.MyEnu", innerClass.getQualifiedName());
		assertEquals("Hello3$MyEnu", innerClass.getTransformedName());
		assertEquals("Hello3$MyEnu", innerClass.getTransformedQualifiedName());

		innerClass = (XClass) i.next();

		// Inner class
		assertEquals("Hello3.InnerClass", innerClass.getQualifiedName());
		assertEquals("Hello3$InnerClass", innerClass.getTransformedName());
		assertEquals("Hello3$InnerClass", innerClass.getTransformedQualifiedName());

		XClass methodInnerClass = (XClass) i.next();

		assertEquals("Hello3.MethodInnerClass", methodInnerClass.getQualifiedName());
		assertEquals("Hello3$MethodInnerClass", methodInnerClass.getTransformedName());
		assertEquals("Hello3$MethodInnerClass", methodInnerClass.getTransformedQualifiedName());
	}

	public void testIsA() throws Exception {
		XClass clazz = _xJavaDoc.getXClass("Hello3");

		assertTrue("Hello3 is Serializable", clazz.isImplementingInterface("java.io.Serializable"));
		assertTrue("Hello3 is MouseListener", clazz.isA("java.awt.event.MouseListener"));
		assertTrue("Hello3 is Action", clazz.isA("javax.swing.Action"));
		assertTrue("Hello3 is AbstractAction", clazz.isA("javax.swing.AbstractAction"));
		assertTrue("Hello3 is TextAction", clazz.isA("javax.swing.text.TextAction"));
	}

	public void testDereferenceProperty() throws Exception {
		XClass clazz = _xJavaDoc.getXClass("Hello3");

		String tagValue = clazz.getDoc().getTag("my:name").getValue();
		String attributeValue = clazz.getDoc().getTag("my:version").getAttributeValue("version");

		assertTrue("Tag Value dereferencing Failed: " + tagValue,
				"this program is called xjavadoc guess why".equals(tagValue));
		assertTrue("Tag Attribute Value dereferencing Failed: " + attributeValue,
				"xjavadoc version is 1.0".equals(attributeValue));
	}

	public void testSupertags() throws Exception {
		XClass clazz = _xJavaDoc.getXClass("Goodbye2");

		assertEquals("xjavadoc.SourceClass", clazz.getClass().getName());

		XMethod getNonsense = clazz.getMethod("getNonsense()");
		Collection tags = getNonsense.getDoc().getTags(true);

		assertEquals(3, tags.size());
	}

	public void testSupermethodsInSource() throws Exception {
		XClass clazz = _xJavaDoc.getXClass("Goodbye2");
		XMethod whatever = clazz.getMethod("whatever(java.lang.String[],int)", false);

		assertEquals(null, whatever);
		whatever = clazz.getMethod("whatever(java.lang.String[],int)", true);
		assertNotNull(whatever);
	}

	public void testSupermethodsInBinary() throws Exception {
		// Turn on super methods
		System.setProperty("xjavadoc.compiledmethods", "true");

		XClass clazz = _xJavaDoc.getXClass("Goodbye2");
		XMethod getClass = clazz.getMethod("getClass()", true);

		System.setProperty("xjavadoc.compiledmethods", "false");

		assertNotNull(getClass);
	}

	public void testNumberOfMethodsInSource() throws Exception {
		XClass hello = _xJavaDoc.getXClass("Hello3");
		XClass goodbye = _xJavaDoc.getXClass("Goodbye2");

		Collection helloMethods = hello.getMethods();
		Collection goodbyeMethods = goodbye.getMethods();
		Collection helloAndGoodbyeMethods = goodbye.getMethods(true);

		assertTrue(11 <= helloMethods.size());
		assertEquals(3, goodbyeMethods.size());
		// we don't inherit the one private method, and one is overridden.
		// assertEquals( 10, helloAndGoodbyeMethods.size() );
	}

	public void testSupermethods2() throws Exception {
		XClass hello [] = { _xJavaDoc.getXClass("Hello2"), 
							_xJavaDoc.getXClass("Hello3") };
		XClass goodbye = _xJavaDoc.getXClass("Goodbye2");

		Collection methods = goodbye.getMethods(true);
		int helloCount = 0;
		int goodbyeCount = 0;

		for (Iterator m = methods.iterator(); m.hasNext();) {
			XMethod method = (XMethod) m.next();

			if (method.getContainingClass() == hello[0]  
				|| method.getContainingClass() == hello[1] 
			) {
				helloCount++;
				if (method.getName().equals("getNonsense")) {
					fail("getNonsense is overridden in Goodbye. Shouldn't get it from Hello3 too.");
				}
			} else if (method.getContainingClass() == goodbye) {
				goodbyeCount++;
			} else {
				String message = "The method " + method.toString() + " was declared in the class "
						+ method.getContainingClass().getName() + " " + "which is an instance of "
						+ method.getContainingClass().getClass().getName() + "@"
						+ method.getContainingClass().hashCode() + ". "
						+ "The method should either come from Hello3 or Goodbye2. " + "Hello3 : "
						+ hello.getClass().getName() + "@" + hello.hashCode() + " " + "Goodbye2 : "
						+ goodbye.getClass().getName() + "@" + goodbye.hashCode();

				fail(message);

			}
		}
		assertEquals(3, goodbyeCount);
		// assertEquals( 7, helloCount );
	}

	public void testAddCommentToUncommentedMethod() throws Exception {
		_xJavaDoc.setUseNodeParser(true);

		XClass clazz = _xJavaDoc.updateMethodTag("Hello3", "noComment()", "here", "is", "a tag for ya", 0);

		File testDir = new File("target/saved/testAddCommentToUncommentedMethod");
		String fileName = clazz.save(testDir);
	}

	public void testBinaryClassHasNoMembersBecauseOfSpeedOptimisation() throws Exception {
		XClass collection = _xJavaDoc.getXClass("java.util.Collection");

		assertEquals(0, collection.getFields().size());
		assertEquals(0, collection.getConstructors().size());
		assertEquals(0, collection.getMethods().size());
	}

	public void testInnerInterface() throws Exception {
		XClass processor = _xJavaDoc.getXClass("hanoi.Processor");
		Iterator innerClasses = processor.getInnerClasses().iterator();
		XClass next = (XClass) innerClasses.next();

		assertEquals("hanoi.Processor.Next", next.getQualifiedName());
		assertEquals("hanoi.Processor$Next", next.getTransformedQualifiedName());
		assertEquals("Processor$Next", next.getTransformedName());
		assertTrue(next.isA("java.io.Serializable"));

		XClass anonClassImplements = (XClass) innerClasses.next();

		assertEquals("java.lang.Object", anonClassImplements.getSuperclass().getQualifiedName());
		assertEquals("java.lang.Object", anonClassImplements.getSuperclass().getTransformedQualifiedName());
		assertEquals("Object", anonClassImplements.getSuperclass().getTransformedName());
		assertTrue(anonClassImplements.isA("hanoi.Processor.Next"));

		XClass anonClassExtends = (XClass) innerClasses.next();

		assertEquals("java.lang.Exception", anonClassExtends.getSuperclass().getQualifiedName());
		assertEquals("java.lang.Exception", anonClassExtends.getSuperclass().getTransformedQualifiedName());
		assertEquals("Exception", anonClassExtends.getSuperclass().getTransformedName());
		assertTrue(anonClassExtends.isA("java.io.Serializable"));
	}

	public void testNoNeedToImportInnerClass() throws Exception {
		XClass processor = _xJavaDoc.getXClass("Goodbye2");
		XMethod gotThis = processor.getMethod("gotThis()");
		XClass processorNext = gotThis.getReturnType().getType();

		assertEquals("hanoi.Processor.Next", processorNext.getQualifiedName());
		assertEquals("hanoi.Processor$Next", processorNext.getTransformedQualifiedName());
		assertEquals("Processor$Next", processorNext.getTransformedName());
	}

	public void testPackageHasInnerClasses() throws Exception {
		XPackage pakkage = _xJavaDoc.getSourcePackage("hanoi");

		assertNotNull(pakkage);
		assertEquals(8, pakkage.getClasses().size());
	}

	public void testUnicode() throws Exception {
		// Should be able to parse a unicode file
		XClass unicode = _xJavaDoc.getXClass("Unicode");
	}

	public void testOldFashioned() throws Exception {
		// We have to tell xjavadoc to get all source classes. otherwise it will think
		// OldFashioned doesn't exist and return an UnknownClass
		_xJavaDoc.getSourceClasses();

		XClass oldFashioned = _xJavaDoc.getXClass("OldFashioned");
		XClass hello = _xJavaDoc.getXClass("Hello3");

		assertEquals(SourceClass.class, oldFashioned.getClass());
		assertTrue("OldFashioned isn't an inner class", !oldFashioned.isInner());

		
		
		// double declared into source "Hello2" and "Hello3"
		// assertEquals( hello.lastModified(), oldFashioned.lastModified() );
	}

	public void testPropertyMethods1() throws Exception {
		XClass hello = _xJavaDoc.getXClass("Hello3");

		XMethod getNonsense = hello.getMethod("getNonsense()");

		assertEquals("nonsense", getNonsense.getPropertyName());
		assertTrue(getNonsense.isPropertyAccessor());
		assertTrue(!getNonsense.isPropertyMutator());

		XMethod setInner = hello.getMethod("setInner(Hello3.InnerClass)");
		log.debug("InnerM=" + setInner);

		assertEquals("inner", setInner.getPropertyName());
		assertTrue(!setInner.isPropertyAccessor());
		assertTrue(setInner.isPropertyMutator());

		XMethod whatever = hello.getMethod("whatever(java.lang.String[][],int)");

		assertNull(whatever.getPropertyName());
		assertTrue(!whatever.isPropertyAccessor());
		assertTrue(!whatever.isPropertyMutator());
	}

	public void testPropertyMethods2() {
		// Get the enclosing class first, just to resolve the rest.
		XClass abAB = _xJavaDoc.getXClass("ab.AB");
		XClass abABC = _xJavaDoc.getXClass("ab.AB.C");

		XMethod getFoo = abABC.getMethod("getFoo()");

		assertTrue(getFoo.isPropertyAccessor());

		XMethod setFoo = abABC.getMethod("setFoo(java.lang.String)");

		assertTrue(setFoo.isPropertyMutator());

		assertSame(getFoo, setFoo.getAccessor());
		assertSame(setFoo, getFoo.getMutator());

		XMethod getBar = abABC.getMethod("getBar()");

		assertTrue(getBar.isPropertyAccessor());

		XMethod setBar = abABC.getMethod("setBar(int)");

		assertTrue(setBar.isPropertyMutator());

		assertNull(getBar.getMutator());
		assertNull(setBar.getAccessor());

		XMethod isThisIsNotAnAccessor = abABC.getMethod("isThisIsNotAnAccessor()");

		assertTrue(!isThisIsNotAnAccessor.isPropertyAccessor());

		XMethod setThisIsAMutator = abABC.getMethod("setThisIsAMutator(boolean[])");

		assertTrue(setThisIsAMutator.isPropertyMutator());

	}

	/**
	 * Test for bugfix for XJD-17
	 */
	public void testXJD17() {
		XClass abAB = _xJavaDoc.getXClass("ab.AB");
		XClass abB = _xJavaDoc.getXClass("ab.B");

		assertEquals(xjavadoc.SourceClass.class, abAB.getClass());
		assertEquals(xjavadoc.SourceClass.class, abB.getClass());
		assertTrue(!abAB.isInterface());
		assertTrue(abB.isInterface());
		assertTrue(!abAB.isInner());
		assertTrue(!abB.isInner());
		assertTrue(!abAB.isAnonymous());
		assertTrue(!abB.isAnonymous());
		assertEquals("ab", abAB.getContainingPackage().getName());
		assertEquals("ab", abB.getContainingPackage().getName());

		// Do some more funny far fetched checking
		XClass abABC = _xJavaDoc.getXClass("ab.AB.C");
		XClass abBD = _xJavaDoc.getXClass("ab.B.D");

		assertEquals(xjavadoc.SourceClass.class, abABC.getClass());
		assertEquals(xjavadoc.SourceClass.class, abBD.getClass());
		assertTrue(abABC.isInterface());
		assertTrue(!abBD.isInterface());
		assertTrue(abABC.isInner());
		assertTrue(abBD.isInner());
		assertTrue(!abABC.isAnonymous());
		assertTrue(!abBD.isAnonymous());
		assertEquals("ab", abABC.getContainingPackage().getName());
		assertEquals("ab", abBD.getContainingPackage().getName());
	}

	public void testXJD64() {
		XClass clazz = _xJavaDoc.getXClass("XJD64Part2");
		assertNotNull(clazz);
		XMethod getErrorCodeMethod = clazz.getMethod("getErrorCode()");
		assertNotNull(getErrorCodeMethod);
		assertEquals("public", getErrorCodeMethod.getModifiers());
		assertEquals("XJD64Part2.Code", getErrorCodeMethod.getReturnType().getType().getName());

		clazz = _xJavaDoc.getXClass("XJD64part1");
		assertNotNull(clazz);
	}

	public void testDoSomethingInJdk8() {
		XClass clazz = _xJavaDoc.getXClass("Hello3");
		assertNotNull(clazz);
		XMethod doSomethingInJdk8 = clazz.getMethod("doSomethingInJdk8()");
		assertNotNull(doSomethingInJdk8);

		XDoc doc = doSomethingInJdk8.getDoc();
		
		String attributeValue = doc.getTag("numbers").getAttributeValue("attr_eight");
		assertNotNull(attributeValue);
		assertEquals("val_eight", attributeValue );
		
		attributeValue = doc.getTag("test").getAttributeValue("function");
		assertNotNull(attributeValue);
		assertEquals("lambda", attributeValue );
		
		attributeValue = doc.getTag("test").getAttributeValue("jdk");
		assertNotNull(attributeValue);
		assertEquals("1.8", attributeValue );
	}
}
