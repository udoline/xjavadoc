/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc;

import java.io.File;
import junit.framework.AssertionFailedError;
import xjavadoc.codeunit.CodeTestCase;

/**
 * This is an example of how to extend CodeTestCase, a very handy extension of
 * JUnit's TestCase class. It is intended to be used to test the output of
 * generators like XDocletImpl, Middlegen, AndroMDA and I'm sure there are
 * more... You want to verify that the code you're generating is ok, don't you?
 *
 * @author    <a href="aslak.hellesoy at bekk.no">Aslak Helles&oslash;y</a>
 * @since   17. oktober 2002
 */
public final class CodeTest extends CodeTestCase
{    
	// the classes are under xjavadoc/build/regular/classes or
	// xjavadoc/build/unicode/classes , so we walk two dirs up.

	private  File dir = getRootDir().getParentFile().getParentFile();
	
	private  File t1 = new File( dir, "test/codeunit/CodeUnit1.java" );
	private  File t2 = new File( dir, "test/codeunit/CodeUnit2.java" );
	private  File t3 = new File( dir, "test/codeunit/CodeUnit3.java" );
	private  File t4 = new File( dir, "test/codeunit/CodeUnit4.java" );

	private void determineDir() {
		
		assertNotNull(dir);
		
		assertNotNull(t1);
		
		if (!t1.canRead()) {
			
			// via gradle ... 
			dir = dir.getParentFile().getParentFile();
			assertNotNull(dir);
			
			t1 = new File( dir, "test/codeunit/CodeUnit1.java" );
			t2 = new File( dir, "test/codeunit/CodeUnit2.java" );
			t3 = new File( dir, "test/codeunit/CodeUnit3.java" );
			t4 = new File( dir, "test/codeunit/CodeUnit4.java" );
			
			assertNotNull(t1);
			assertNotNull(t2);
			assertNotNull(t3);
			assertNotNull(t4);
		}
	}
	
	public void testT1SameApiAsT2() throws Exception
	{
		determineDir();
		assertApiEquals( t1, t2 );
	}

	public void testT1DifferentAstFromT2() throws Exception
	{
		try
		{
			determineDir();
			assertAstEquals( t1, t2 );
			fail( "The ASTs should not be equal" );
		}
		catch( AssertionFailedError e )
		{
			assertNotNull(e);
		}
	}

	public void testT3SameApiAsT4() throws Exception
	{
		determineDir();
		assertApiEquals( t3, t4 );
	}
}
