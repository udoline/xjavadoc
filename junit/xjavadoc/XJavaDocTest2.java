/*
 * Copyright (c) 2001-2003 The XDoclet team
 * All rights reserved.
 */
package xjavadoc;

import junit.framework.*;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.HashMap;
import java.util.List;

import xjavadoc.filesystem.FileSourceSet;

/**
 * @since   12.06.2005
 */
public class XJavaDocTest2 extends TestCase
{
    private final XJavaDoc _xJavaDoc = new XJavaDoc();

	public void setUp() throws Exception
	{
		File dir = new File(  "test" );
		HashMap propertyMap = new HashMap();

		propertyMap.put( "name", "xjavadoc" );
		propertyMap.put( "version", "1.5" );

		_xJavaDoc.reset(true);
		_xJavaDoc.setPropertyMap( propertyMap );
		_xJavaDoc.addSourceSet( new FileSourceSet( dir ) );
	}

	/**
	 * A unit test for JUnit
	 */
	public void testReadHello()
	{
		XClass clazz = _xJavaDoc.getXClass( "Misc" );

		XDoc doc = clazz.getDoc();
	}


	public void testXJD58() {

        XClass clazz = _xJavaDoc.getXClass( "XJD58" );
        List ic = clazz.getInnerClasses();
        assertEquals(ic.size(), 1);

    }
}
