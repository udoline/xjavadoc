public class XJD64Part2 extends Exception
{

    public enum Code
    {
        EXAMPLE("Example text");
        String text = "";

        Code(String t)
        {
            this.text = t;
        }

        public String getText()
        {
            return this.text;
        }
    }

    private Code errorCode;

    public XJD64Part2(Code ec)
    {
        errorCode = ec;
    }

    public Code getErrorCode()
    {
        return this.errorCode;
    }

    public String getMessage()
    {
        return this.errorCode.getText();
    }
}