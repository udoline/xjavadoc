// no package

import java.io.*;
import java.util.*;
import java.rmi.*;
import annotation.TestAnnotation;
import java.lang.annotation.*;

/**
 * Bla bla yadda yadda
 * 
 * @foo:bar beer="good" tea="bad"
 *
 * @my:name this program is called ${name} guess why
 * @my:version version="${name} version is ${version}"
 *
 */
class Hello3 extends javax.swing.text.TextAction<DS,C extends CI,?>implements javax.swing.event.MouseInputListener,Remote<T>,Serializable
{


      public enum Planet {
        MERCURY (3.303e+23, 2.4397e6),
        VENUS   (4.869e+24, 6.0518e6),
        EARTH   (5.976e+24, 6.37814e6),
        MARS    (6.421e+23, 3.3972e6),
        JUPITER (1.9e+27,   7.1492e7),
        SATURN  (5.688e+26, 6.0268e7),
        URANUS  (8.686e+25, 2.5559e7),
        NEPTUNE (1.024e+26, 2.4746e7),
        PLUTO   (1.27e+22,  1.137e6),

        PLUS   { double eval(double x, double y) { return x + y; } },
        MINUS  { double eval(double x, double y) { return x - y; } },
        TIMES  { double eval(double x, double y) { return x * y; } },
        DIVIDE { double eval(double x, double y) { return x / y; } }

        ;

        abstract double eval(double x, double y);


        private final double mass;   // in kilograms
        private final double radius; // in meters
        Planet(double mass, double radius) {
            this.mass = mass;
            this.radius = radius;
        }
        private double mass()   { return mass; }
        private double radius() { return radius; }

        // universal gravitational constant  (m3 kg-1 s-2)
        public static final double G = 6.67300E-11;

        double surfaceGravity() {
            return G * mass / (radius * radius);
        }
        double surfaceWeight(double otherMass) {
            return otherMass * surfaceGravity();
        }
    };

    public static enum Flag {;

        public static String getDBValue(boolean indicator) {
            String key = null;
            if (indicator) {
                key = CHAR_TRUE;
            } else {
                key = CHAR_FALSE;
            }

            return key;
        }

        public static boolean getFlagType(String indicator) {
            boolean result = false;

            if (indicator != null)
                if (indicator.equals(CHAR_TRUE)) {
                    result = true;
                }
            return result;
        }

        public static boolean getFlagType(String indicator, boolean defaultValue) {
            boolean result = false;

            if (indicator != null)
                if (indicator.equals(CHAR_TRUE)) {
                    result = true;
                } else if (indicator.equals(CHAR_FALSE)) {
                    result = false;
                } else {
                    result = defaultValue;
                }
            return result;
        }

        public static final String CHAR_FALSE = "F";
        public static final String CHAR_TRUE = "T";
   };

	private List lst2 = new ArrayList();

	private static final List<String> lst = new ArrayList<String>();

	private static final List<String> lst3 = new ArrayList();

	private Map map = new HashMap();

	private Map<String, List> mapJdk14 = new HashMap();

	private Map<String, List> mapJdk15 = new HashMap<String, List>();

	private Map<String, List> mapJdk17 = new HashMap<>();

	enum MyEnu {
		One, Two, Three
	};

	/**
	 * This shouldn't be the first sentence.This one should be. Is everything OK?
	 */
	@MyAnnotation()
	public void firstMethod() {

		for (int i = 0; i < 10; ++i) {
			;
		}

		List items = new ArrayList();
		items.add("1");
		items.add("2");
		items.add("3");
		items.add("4");

		for (final Object item : items) {
			System.out.println("Has item");
		}

		for (Object item : items) {
			System.out.println("Has item");
		}

	}; // The spec says semicolon is illegal here, but javac accepts it, and xjavadoc
		// will too.

	// Test Jdk13 ...
	private List getMapList() {
		return new ArrayList();
	}

	// Test Jdk14 ...
	private List<Map> getMapListJdk14() {
		return new ArrayList();
	}

	// Test Jdk15 ...
	private Map<String, ?> getMap(List<String> params, Map<Integer, Object> map) {
		return new HashMap<params, Map>(111);
	}

	// Test Jdk17 ...
	private Map<String, Map> getMapJdk17(List<String> params, Map<Integer, Object> map) {
		return new HashMap<>();
	}

	/**
	 * do something in Jdk 1.8
	 *
	 * @numbers one="one" attr_eight="val_eight"
	 * 
	 * @test function="lambda" jdk="1.8"
	 */
	@MyAnnotation(name = "aName", intValue = 18, names = { "bName", "...", "nName" })
	public void doSomethingInJdk8() {
		List<Integer> numbers = new ArrayList<>();
		numbers.add(-666);
		numbers.forEach((n) -> {
			System.err.println(n);
		});
	}

	public void testVaArgs(Object... args) {

	}

	public void enumTest(int x, MyEnum me) {

	}

	/**
	 * There are truble in the old version of XDoclet with the
	 * <b>CHARACTER_LITERAL</b> like <code>"'"</code> ...
	 * 
	 * @param args more
	 */
	public void testCharacterLiteral(Object... args) {
		String l = '\'', 
				m = "'",
				n = "\\'",
				o = '\\';
		String r = ' ';
		r += m + ' ';
		r += n + ' ';
		r += l + ' ';
		r += o + ' ';
		args[0] = r;
	}

	// public Map hm = new HashMap();

	/**
	 * Braba papa, barba mama, baraba brother, barba sister
	 */
	private final String privateField = "barba papa";

	protected final String protectedField;

	public final String publicField;

	/**
	 * Blabla. Do you like Norwegian letters? �&#x00F8;��&#x00F8;�.
	 *
	 * @foo
	 */
	public Hello3() {
	}

	/**
	 * Yadda yadda
	 *
	 * @bar
	 */
	Hello( File f ) {
	}

	protected Hello( String f ) throws java.io.IOException, InvalidDefinitionException {
	}
	// what can you do about this comment?

	/**
	 * This is getNonsense.
	 *
	 * @star:wars is="a crappy movie" but="I went to see it anyway"
	 *
	 * @empty:tag
	 */
	public InputStream getNonsense() {
		return null;
	}

	/**
	 * Mr. Jones
	 *
	 * @more testdata, bla bla
	 * @maybe this="is" only="testdata"
	 */
	protected void whatever(String[] s[], int i) {
		// assert true;
		// assert true:"blabla";
	}

	/**
	 * What Ever
	 * 
	 * @more howdy, bla bla
	 * @numbers one="two" three="four"
	 */
	private void whatever(String[] s, int i) {
	}

	Long noComment() {
	}

	public void setInner(InnerClass inner) {
	}

	public class InnerClass extends java.lang.Object {

		private final String doodoo = "doodoo";

		private String justForFun() {
			return "justForFun";
		}
	}

	public void methodBlockInnerClass() {
		class MethodInnerClass extends Object {
			/**
			 * What Ever
			 * 
			 * @more howdy, bla bla
			 * @numbers one="two" three="four"
			 */
			public void haha() {
				System.out.println("haha");
			}
		}
		MethodInnerClass methodInner = new MethodInnerClass();
		methodInner.haha();
	}
}

class OldFashioned {
	private String blah;

	public class InnerInOldFashioned {
		private String duh;
	}

}

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation {

	String value()

	default "";

	String name();

	int intValue();

	String[] names();
}
//Test XJD-27 no trailing end of line 