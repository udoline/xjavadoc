package codeunit;

// take a look at CodeUnit2.java
import java.io.Serializable;

class CodeUnit extends java.awt.Component implements Serializable {
	static { int i; }

	void foo() {
		Class c = int[].class;
		Class d = Object[].class;
		t.new T();
		((T)t).method();
		this( (int) (r * 255), (int) (g * 255));
		return "[i=" + (value) + "]";
		int q = (int)+3;
		int z = (int)4;
		int y = (z)+5;
		String s = (String) "ff";
		String t = (s)+"blort";
	}
}
