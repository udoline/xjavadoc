package codeunit;

// take a look at CodeUnit4.java
import java.io.Serializable;

interface Bar extends Serializable {
    public void blah(String s);
}
