/*
 * Copyright (c) 2005 Your Corporation. All Rights Reserved.
 */

/**
 * @author Adamansky Anton (anton@adamansky.com)
 */

public class XJD58 {

    public class Outer {

        private class Inner extends InnerTop {

            public void innerMethod() {
                new Runnable() {
                    public void run() {
                        Inner.super.invoke(); // Parse exception here ?. at the dot before super
                    }
                };
            }
        }
    }
}
